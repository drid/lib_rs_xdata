#include "../inc/rs_xdata.h"
#include <stdio.h>
#include <assert.h>
#include <string.h>
int main() {
  int res;
  char datastring[LIB_RS_DATA_PAYLOAD_SIZE];
  // Test int format
  res = lib_rs_xdata_format(datastring, 1, 1, 2, 0x3dd4, 8, 0xaf, 4);
  assert(strcmp(datastring, "xdata=010100003dd400af\r\n") == 0);
  // Test daisy chain increment
  res = lib_rs_xdata_chain_increase(datastring);
  assert(strcmp(datastring, "xdata=010200003dd400af\r\n") == 0);
  // Test daisy chain overflow
  res = lib_rs_xdata_format(datastring, 1, 0xff, 2, 0x3dd4, 8, 0xaf, 4);
  res = lib_rs_xdata_chain_increase(datastring);
  assert(strcmp(datastring, "xdata=010000003dd400af\r\n") == 0);
}
