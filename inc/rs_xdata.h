/*!
   \file "rs_xdata.h"
   \brief "Header file for XDATA protocol library"
   \author "Ilias Daradimos"
   \date "19"/"02"/"2022"
*/
#ifndef LIB_RS_DATA_PAYLOAD_SIZE
#define LIB_RS_DATA_PAYLOAD_SIZE 80
#endif

int lib_rs_xdata_format(char *buffer, unsigned char instr_id, unsigned char dc_index,int arglen, ...);
int lib_rs_xdata_chain_increase(char *buffer);
