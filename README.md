# lib_rs_xdata
Radiosonde XDATA format library
Formats integer values in according to XDATA protocol

## Usage
Each value set provided to **lib_rs_xdata_format** consists of an integer value followed by the minimum number of hex digits that are to be used.

* **datastring** is a pointer to a char arrays where the formatted data will be written.
* ID: 1
* Daisy Chain index: 1
* Value sets: 2
* Value 1: 0x3dd4, 8 hex digits
* Value 1: 0xaf, 4 hex digits
```C
lib_rs_xdata_format(xdatastring, 1, 1, 2, 0x3dd4, 8, 0xaf, 4);
```

**lib_rs_xdata_chain_increase** increases the daisy chain number of a XDATA formatted string

```C
new_idx = lib_rs_xdata_chain_increase(xdatastring)
```
Index is updated in xdatastring and new value is also returned
