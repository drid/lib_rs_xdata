/*!
   \file "rs_xdata.c"
   \brief "Source file for XDATA protocol library"
   \author "Ilias Daradimos"
   \date "19"/"02"/"2022"
*/
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include "../inc/rs_xdata.h"

/*!
   \brief "Format data according to XDATA protocol"
   \param buffer XDATA string pointer
   \param instr_id Instrument ID
   \param dc_index Daisy chain index
   \param arglen Count of passed parameter sets
   \param ... Parameter set (value, digits)
   \return length of formatted string
*/
int lib_rs_xdata_format(char *buffer, unsigned char instr_id, unsigned char dc_index,int arglen, ...) {
  char tmp[10];
  va_list valist;
  arglen *= 2;
  snprintf(buffer, LIB_RS_DATA_PAYLOAD_SIZE, "xdata=%02x%02x", instr_id, dc_index);
  va_start(valist, arglen);
  for (int argnum = 0; argnum < arglen; argnum+=2) {
    snprintf(tmp, 10, "%.*x", va_arg(valist, int), va_arg(valist, int));
    strcat(buffer, tmp);
  }
  strcat(buffer, "\r\n");
  va_end(valist);
  return strlen(buffer);
}

/*!
   \brief "Increment daisy chain number"
   \param buffer XDATA string pointer
   \return New daisy chain index
*/
int lib_rs_xdata_chain_increase(char *buffer) {
  unsigned int chain_index, id;
  char str_idx[3];
  sscanf(buffer, "xdata=%02x%02x", &id, &chain_index);
  chain_index++;
  sprintf(str_idx, "%02x", chain_index & 0xff);
  buffer[8] = str_idx[0];
  buffer[9] = str_idx[1];
  return chain_index;
}
